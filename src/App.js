import React, {Component} from "react";
import "./App.css";
import Card from "./Card/Card";
import WinCalculator from './WinCalculator'
const rank = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'j', 'q', 'k', 'a'];
const suit = ['D', 'H', 'C', 'S'];




class App extends Component {
    state={
      cards:[],
        combo:""
    };

    shuffleCards = () => {
        const allCards = [];
        for (let i = 0; i < suit.length; i++) {
            for (let k = 0; k < rank.length; k++) {
                let oneCard = {suit: suit[i], rank: rank[k]};
                allCards.push(oneCard);

            }
        }


        let fiveCards = [];
        for (let i = 0; i < 5; i++) {
            let oneRandomCard = Math.floor(Math.random() * (allCards.length - 1) + 1);
            fiveCards.push(allCards[oneRandomCard]);
            allCards.splice(oneRandomCard,1);
            console.log(oneRandomCard);
        }
        const combo=new WinCalculator(fiveCards).getBestHand();

        console.log(fiveCards,allCards);
        this.setState({cards:fiveCards,combo:combo});
    };

    render() {
        return (
            <div className="App playingCards faceImage">
                <button onClick={this.shuffleCards}>GiveMeCards!!!</button>
                <ul className="table">
                    {
                        this.state.cards.map((card,index)=>{
                            return(

                                    <Card rank={card.rank} suit={card.suit} key={index}/>

                            )
                        })
                    }

                </ul>
                <div className="myCombo">{this.state.combo}</div>
            </div>
        );
    }
}

export default App;
