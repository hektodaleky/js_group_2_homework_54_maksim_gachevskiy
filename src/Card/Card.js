/**
 * Created by Max on 19.01.2018.
 */
/**
 * Created by Max on 17.01.2018.
 */
import React from "react";
const suit = {
    'D': {class:'diams',ico:"♦"},
    'H': {class:'hearts',ico:"♥"},
    'C': {class:'clubs',ico:"♣"},
    'S': {class:'spades',ico:"♠"}
};
const Card = (props) => {
    const cardClass=`card rank-${props.rank.toLowerCase()} ${suit[props.suit].class}`;
    return (
        <li>
        <div className={cardClass}>
            <span className="rank">{props.rank.toUpperCase()}</span>
            <span className="suit">{suit[props.suit].ico}</span>



        </div></li>
);
};
export default Card;