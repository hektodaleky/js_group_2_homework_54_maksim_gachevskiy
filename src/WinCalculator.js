class WinCalculator {
    constructor(cards) {
        this.cards = cards;
        this.suits = this.cards.map(card => card.suit);
        this.ranks = this.cards.map(card => card.rank);

        this.isFlush = this.suits.every(suit => suit === this.suits[0]);
    }

    isRoyalFlush() {
        return this.isFlush && this.ranks.includes('10') &&
            this.ranks.includes('J') &&
            this.ranks.includes('Q') &&
            this.ranks.includes('K') &&
            this.ranks.includes('A')
    }


    showAccumulator() {
        const ranksNumber = {};
        let twoPair = 0;
        this.ranks.forEach(rank => {
            if (!ranksNumber[rank])
                ranksNumber[rank] = 1;
            else ranksNumber[rank]++;
        });

        for (let i in ranksNumber) {
            if (ranksNumber[i] === 2)
                twoPair++;
        }

        if (Object.values(ranksNumber).includes(4))
            return "Four of a kind";
        else if (Object.values(ranksNumber).includes(3) && Object.values(ranksNumber).includes(2))
            return "Full house";
        else if (Object.values(ranksNumber).includes(3))
            return "Three of a kind";
        else if (twoPair > 1)
            return "Two Pair";
        else if (Object.values(ranksNumber).includes(2))
            return "Pair";
        console.log(ranksNumber);
    }

    getBestHand() {
        if (this.isRoyalFlush())
            return 'Royal Flush';
        else if (this.isFlush)
            return 'Flush';

        else if (this.showAccumulator() === "Pair")
            return this.showAccumulator();
        else if (this.showAccumulator() === "Three of a kind")
            return this.showAccumulator();
        else if (this.showAccumulator() === "Full house")
            return this.showAccumulator();
        else if (this.showAccumulator() === "Two Pair")
            return this.showAccumulator();
        else
            return 'Nothing'
    }
}
export default WinCalculator;