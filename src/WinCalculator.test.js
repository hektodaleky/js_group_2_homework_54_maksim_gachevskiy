import WinCalculator from "./WinCalculator";
const royalFlush=[
    {suit:'H',rank:'Q'},
    {suit:'H',rank:'A'},
    {suit:'H',rank:'K'},
    {suit:'H',rank:'J'},
    {suit:'H',rank:'10'}
];
const flush=[
    {suit:'H',rank:'2'},
    {suit:'H',rank:'A'},
    {suit:'H',rank:'5'},
    {suit:'H',rank:'7'},
    {suit:'H',rank:'10'}
];
const pair =[
    {suit:'H',rank:'Q'},
    {suit:'D',rank:'A'},
    {suit:'C',rank:'K'},
    {suit:'S',rank:'Q'},
    {suit:'H',rank:'10'}
];
const twoPair =[
    {suit:'H',rank:'Q'},
    {suit:'D',rank:'A'},
    {suit:'C',rank:'K'},
    {suit:'S',rank:'Q'},
    {suit:'H',rank:'A'}
];
const three =[
    {suit:'H',rank:'Q'},
    {suit:'D',rank:'A'},
    {suit:'C',rank:'Q'},
    {suit:'S',rank:'Q'},
    {suit:'H',rank:'10'}
];
const full =[
    {suit:'H',rank:'Q'},
    {suit:'D',rank:'10'},
    {suit:'C',rank:'Q'},
    {suit:'S',rank:'Q'},
    {suit:'H',rank:'10'}
];
it('should Royal flush',()=>{
    const calc=new WinCalculator(royalFlush);
    const result=calc.getBestHand();
    expect(result).toEqual('Royal Flush')
});
it('should Flush',()=>{
    const calc=new WinCalculator(flush);
    const result=calc.getBestHand();
    expect(result).toEqual('Flush')
});
it('should Pair',()=>{
    const calc=new WinCalculator(pair);
    const result=calc.getBestHand();
    expect(result).toEqual('Pair')
});
it('should Tree of a king',()=>{
    const calc=new WinCalculator(three);
    const result=calc.getBestHand();
    expect(result).toEqual('Three of a kind')
});
it('should Full house',()=>{
    const calc=new WinCalculator(full);
    const result=calc.getBestHand();
    expect(result).toEqual('Full house')
});
it('should Two Pair',()=>{
    const calc=new WinCalculator(twoPair);
    const result=calc.getBestHand();
    expect(result).toEqual('Two Pair')
});